# Hello GitLab CI/CD!

이 프로젝트는 인포그랩이 제작한 CI/CD 기초 가이드 입니다.  

본 가이드에서는 **GitLab CI/CD Pipeline**이 작동하는 방식과 GitLab CI/CD에 대한 특정 지침을 구성하는 YAML 파일인 `.gitlab-ci.yml`의 구성 요소를 이해할 수 있도록 작성되었습니다.  

특정 프로그램에 구애받지 않고 GitLab CI/CD Pipeline의 구성 요소와 개념을 이해할 수 있도록 소스 코드 없이 Job 로그에 텍스트를 출력하는 방식의 Demo입니다.  

파이프라인을 구조화하는 세 가지 주요 방법이 있으며, 각각 고유한 장점이 있습니다. 필요한 경우 이러한 방법을 혼합하고 일치시킬 수 있습니다.  

* `Basic` : 모든 구성이 찾기 쉬운 한 곳에 있는 간단한 프로젝트에 적합합니다.
* `Directed Acyclic Graph` : 효율적인 실행이 필요한 크고 복잡한 프로젝트에 적합합니다.
* `Parent/Child 파이프라인` : 독립적으로 정의된 많은 구성 요소가 있는 단일 저장소 및 프로젝트에 적합합니다.

## 사전 조건

**Settings > CI/CD**의 Runners 섹션에서 프로젝트에 대해 비슷한 성능의 Runner가 2개 이상 활성화되어 있는지 확인이 필요합니다.

Runner가 없는 경우 GitLab Runner를 설치하고 등록해야 합니다.

## [Demo 1] Basic(기본) 파이프라인

GitLab에서 가장 간단한 파이프라인입니다. 빌드 단계의 모든 Job을 동시에 실행하고, 모든 Job이 완료되면 테스트 단계의 모든 Job을 동일한 방식으로 실행합니다. 가장 효율적인 방법은 아니며, 단계가 많으면 상당히 복잡해질 수 있지만, 유지 관리가 더 쉽습니다.

```mermaid
graph LR
  subgraph deploy stage
    deploy --> deploy_a
    deploy --> deploy_b
  end
  subgraph test stage
    test --> test_a
    test --> test_b
  end
  subgraph build stage
    build --> build_a
    build --> build_b
  end
  build_a -.-> test
  build_b -.-> test
  test_a -.-> deploy
  test_b -.-> deploy
```

### 실행 방법

[Web IDE] 버튼을 클릭하고 [.gitlab-ci.yml](.gitlab-ci.yml) 파일을 [sample/.gitlab-ci_basic.yml](sample/.gitlab-ci_basic.yml) 파일의 내용으로 수정하고 커밋합니다.

### 결과 확인

**CI/CD > Pipelines**에서 실행 중인 파이프라인을 클릭하면, 아래와 같이 각 단계의 모든 Job이 완료되어야 다음 단계의 Job이 실행되는 것을 확인할 수 있습니다.

![Basic Pipelines](images/basic_pipeline.gif "Basic Pipelines")

## [Demo 2] Directed Acyclic Graph(방향성 비순환 그래프) 파이프라인

효율성이 중요하고 모든 것이 가능한 한 빨리 실행되기를 원한다면 DAG(Directed Acyclic Graphs)를 사용할 수 있습니다. `needs` 키워드를 사용하여 Job 사이의 의존 관계를 정의할 수 있습니다. GitLab이 Job 간의 관계를 알고 있으면, 가능한 한 빨리 모든 것을 실행할 수 있으며, 가능한 경우 후속 단계로 건너뛸 수도 있습니다.

```mermaid
graph LR
  subgraph Pipeline using DAG
    build_a --> test_a --> deploy_a
    build_b --> test_b --> deploy_b
  end
```

### 실행 방법

[Web IDE] 버튼을 클릭하고 [.gitlab-ci.yml](.gitlab-ci.yml) 파일을 [sample/.gitlab-ci_directed-acyclic-graph.yml](sample/.gitlab-ci_directed-acyclic-graph.yml) 파일의 내용으로 수정하고 커밋합니다.

### 결과 확인

**CI/CD > Pipelines**에서 실행 중인 파이프라인을 클릭하면, 빌드 단계의 build_b Job이 완료되지 않았지만 test_a Job이 실행되며, 테스트 단계의 test_b Job이 완료되지 않았음에도 바로 deploy_a Job이 실행되는 것을 확인할 수 있습니다.

![Directed Acyclic Graph Pipelines](images/dag_pipeline.gif "Directed Acyclic Graph Pipelines")

## [Demo 3] Parent/Child(상위/하위) 파이프라인

위의 Demo에서, 독립적으로 빌드되는 두 가지 유형이 있음을 알 수 있습니다. 이것은 `trigger` 키워드를 통해 상위/하위 파이프라인을 사용하는 데 이상적입니다. 구성을 여러 파일로 분리하여 매우 간단하게 유지합니다. 이 기능은 다음과 결합할 수도 있습니다.

* `rules` 키워드 : 예를 들어, 하위 파이프라인은 `changes` 키워드로 지정한 영역에 변경이 있을 때만 트리거 됩니다.
* `include` 키워드 : 하나의 긴 `.gitlab-ci.yml` 파일을 여러 파일로 분할하여 가독성을 높이거나 여러 위치에서 동일한 구성의 중복을 줄일 수 있습니다.
* 하위 파이프라인 내부의 DAG 파이프라인으로 두 가지 이점을 모두 얻을 수 있습니다.

```mermaid
graph LR
  subgraph Parent pipeline
    trigger_a -.-> build_a
  trigger_b -.-> build_b
    subgraph child pipeline B
    build_b --> test_b --> deploy_b
    end

    subgraph child pipeline A
      build_a --> test_a --> deploy_a
    end
  end
```

### 실행 방법

[Web IDE] 버튼을 클릭하고 [.gitlab-ci.yml](.gitlab-ci.yml) 파일을 [sample/.gitlab-ci_parent-child.yml](sample/.gitlab-ci_parent-child.yml) 파일의 내용으로 수정한 후, a 디렉토리와 b 디렉토리에 각각 파일 하나씩을 추가하고 커밋합니다.

### 결과 확인

**CI/CD > Pipelines**에서 실행 중인 파이프라인을 클릭하면, trigger_a, trigger_b Job이 각각 하위 파이프라인을 트리거 하는 것을 알 수 있으며, Downstream에서 각 child-pipeline의 `>`을 클릭하면 하위 단계 및 Job이 실행된 것을 확인할 수 있습니다.

![Parent/Child Pipelines](images/parent-child_pipeline.gif "Parent/Child Pipelines")
